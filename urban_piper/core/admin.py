from django.contrib import admin
from urban_piper.core.models import *

admin.site.register(DeliveryTask)
admin.site.register(DeliveryTaskState)
admin.site.register(DeliveryStateTransition)
